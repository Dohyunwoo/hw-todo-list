import Vue from 'vue'
import VueCompositionApi, { ref, reactive, computed } from '@vue/composition-api'

Vue.use(VueCompositionApi) 

const todoService = reactive({
  list: [],
  all: computed(() => {
    return todoService.list
  }),
  complete: computed(() => {
    return todoService.list.filter(e => e.complete)
  }),
  active: computed(() => {
    return todoService.list.filter(e => !e.complete)
  }),
  purge: function() {
    todoService.complete.length
      ? todoService.list = todoService.active
      : alert('완료된 목록이 없습니다.') 
  }
})

export default todoService
