import Vue from 'vue'
import VueCompositionApi, { reactive, computed } from '@vue/composition-api'

Vue.use(VueCompositionApi) 

const filterService = reactive({
  condition: -1,
  filterBy: function(item) {
    if (filterService.condition == -1) return true;
    return !!filterService.condition == item.complete;
  },
  conditionTokorean: computed(() => {
    return filterService.condition == -1 ? '전부'
      : ( filterService.condition ? '완료' :'아직')
  })
})

export default filterService
